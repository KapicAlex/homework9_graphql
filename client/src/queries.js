import gql from 'graphql-tag';

export const MESSAGES_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput,$filter:String,$skip:Int,$first:Int){
  messages(orderBy: $orderBy,filter:$filter,skip:$skip, first:$first) {
    messageList {
      id
      body
      likes
      dislikes
      replies {
        id
        body
      }
    }
  }
}
`;

export const POST_MESSAGE_MUTATION = gql`
    mutation PostMutation($body: String!) {
      postMessage(body: $body) {
        id
        body
        likes
        dislikes
        replies {
          id
          body
        }
    }
}
`;

export const POST_REPLY_MUTATION = gql`
    mutation PostMutation($messageId: ID!, $body: String!) {
        postReply(messageId: $messageId, body: $body) {
            id
            body
        }
    }
`;

export const LIKE_MUTATION = gql`
  mutation PostMutation($messageId: ID!) {
    likeMessage(messageId: $messageId) {
      id
      body
      likes
      dislikes
      replies {
        id
        body
      }
    }
  }
`;

export const DISLIKE_MUTATION = gql`
  mutation PostMutation($messageId: ID!) {
    dislikeMessage(messageId: $messageId) {
      id
      body
      likes
      dislikes
      replies {
        id
        body
      }
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
subscription {
  newMessage {
    id
    body
    likes
    dislikes
    replies {
      id
      body
    }
  }
}
`;

export const NEW_MESSAGE_REACTION_SUBSCRIPTION = gql`
subscription {
    newMessageReaction {
      id
      body
      likes
      dislikes
      replies {
        id
        body
      }
    }
  }
`;