import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { LIKE_MUTATION, DISLIKE_MUTATION } from '../../queries';
import Reply from '../Reply/Reply';
import ReplyInput from '../ReplyInput/ReplyInput';
import './message.css';

const Message = ({ message }) => {
  const [isModalShown, setIsModalShown] = useState(false);
  const { body, likes, dislikes } = message;
  const anonimId = message.id.substring(0, 4)
  const messageId = message.id;

  const showModal = () => setIsModalShown(!isModalShown);

  return (
    <div className="message">
      <div className="message-header">
        <div className="message-user-name">#{anonimId}</div>
        <button type="button" className="messageReply" onClick={() => setIsModalShown(true)}>Reply</button>
      </div>
      
      <div className="message-main">
        <div className="message-body">{body}</div>
        <div className="message-reactions">

          <Mutation
            mutation={LIKE_MUTATION}
            variables={{messageId}}
            >
              {postMutation =>  <button className="message-like" onClick={postMutation}>
                                  <i className="fas fa-thumbs-up"></i>
                                </button>
              }
          </Mutation>
          <div className="message-like-decimal">{likes}</div>

          <Mutation
            mutation={DISLIKE_MUTATION}
            variables={{messageId}}
            >
              {postMutation =>  <button className="message-like" onClick={postMutation}>
                                  <i className="fas fa-thumbs-down"></i>
                                </button>
              }
          </Mutation>
          <div className="message-like-decimal">{dislikes}</div>

        </div>
      </div>
      <div className="message-replies">
        {message.replies && message.replies.map(reply => (
          <Reply key={reply.id} reply={reply} />
        ))}
      </div>
      {isModalShown && <ReplyInput showModal={showModal} messageId={messageId} /> } 
    </div>
  )
}

export default Message;