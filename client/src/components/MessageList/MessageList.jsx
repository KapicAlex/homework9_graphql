import React, { useState } from 'react';
import { Query } from 'react-apollo';
import Message from '../Message/Message';
import MessageInput from '../MessageInput/MessageInput'
import { MESSAGES_QUERY, NEW_MESSAGE_SUBSCRIPTION, NEW_MESSAGE_REACTION_SUBSCRIPTION } from '../../queries';
import './messageList.css'

const MessageList = () => {
  let [filter, setFilter] = useState(null);
  let [inputFilter, setInputFilter] = useState("");
  let [skip, setSkip] = useState(null);
  let [first, setFirst] = useState(null);
  let [orderBy, setOrder] = useState('createdAt_DESC');
  const orders = [
        'createdAt_DESC',
        'createdAt_ASC',
        'likes_ASC',
        'likes_DESC',
        'dislikes_ASC',
        'dislikes_DESC']

  const _subscribeToNewMessage = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if(!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
        if(exists) return prev;

        return {
          ...prev, messages: {
              messageList: [...prev.messages.messageList, newMessage],
              count: prev.messages.messageList.length + 1,
              __typename: prev.messages.__typename
          }
        };

      }
    });
  };

  const _subscribeToNewMessageReaction = subscribeToMore => {
    subscribeToMore({
        document: NEW_MESSAGE_REACTION_SUBSCRIPTION,
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data) return prev;
          const { reactMessage } = subscriptionData.data;
          const messageList = prev.messages.messageList.map(message => message.id === reactMessage.id ? reactMessage : message);
          return {
              ...prev, messages: {
                  messageList: [...messageList],
                  count: prev.messages.messageList.length,
                  __typename: prev.messages.__typename
              }
          }
        }
    });
  };

  return (
    <Query query={MESSAGES_QUERY} variables={{ orderBy, filter, skip, first }}>
      {({ loading, error, data, subscribeToMore }) => {
        if(loading) return <div>Loading... Please wait.</div>;
        if(error) return <div>Oops....Fetch error</div>;

        _subscribeToNewMessage(subscribeToMore);
        _subscribeToNewMessageReaction(subscribeToMore);

        const { messages: { messageList } } = data;

        return (
          <div>
            <div className="sort-select"> Sorted by:
                        <select value={orderBy} onChange={e => setOrder(e.target.value)}>
                          {orders.map(order => {
                            return <option key={order} value={order}>{order}</option>
                          })}
                        </select>
            </div>
            <div className="filter">
              <input
                  placeholder='filter'
                  value={inputFilter}
                  onChange={e => setInputFilter(e.target.value)} />
              <button onClick={() => setFilter(inputFilter)}>Filter</button>
            </div>
            <div className="pagination">Pagination
              <input
                  type="number"
                  value={skip || 0}
                  min="0"
                  max={messageList.length}
                  onChange={e => setSkip(parseInt(e.target.value))} />
              <input
                  type="number"
                  value={first || messageList.length}
                  min="0"
                  max={messageList.length}
                  onChange={e => setFirst(parseInt(e.target.value))} />
            </div>
            <div className="messageList">
            {messageList.map(message => <Message key={message.id} message={message} />)}
            </div>
            <MessageInput />
          </div>
          
        );

      }}
    </Query>

  )
};

export default MessageList;