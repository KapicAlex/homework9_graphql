import React from 'react';
import './reply.css'

const Reply = ({ reply }) => {
  const id = reply.id.substring(0, 4);
  
  return (
      <div className="reply">
        <div className="reply-id">#{id}</div>
        <div className="reply-text">{reply.body}</div>
      </div>
  )

}

export default Reply;