import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { MESSAGES_QUERY,  POST_MESSAGE_MUTATION } from '../../queries';
import "./messageInput.css";

const MessageInput = () => {
  const [body, setBody] = useState("");

  const _updateStoreAfterAddingMessage = (store, newMessage) => {
    const orderBy = 'createdAt_DESC';
    const data = store.readQuery({
        query: MESSAGES_QUERY,
        variables: {
            orderBy
        }
    });
    data.messages.messageList.unshift(newMessage);
    store.writeQuery({
        query: MESSAGES_QUERY,
        data
    });
};

  return (
    <div className="message-input" >

      <Mutation
      mutation={POST_MESSAGE_MUTATION}
      variables={{body}}
      update={( store, { data: { postMessage } }) => {
        _updateStoreAfterAddingMessage(store, postMessage)
      }}
      onCompleted={() => {setBody("")}}
      >
      {postMutation => <form onSubmit={postMutation}>
                          <input type="text"
                           className="message-input-text"
                           placeholder="Enter your message"
                           value={body}
                           onChange={e => setBody(e.target.value)} />
                          <button className="message-input-button"
                           type="submit">Send</button>
                        </form>
      }
      </Mutation>

    </div>
  );
};

export default MessageInput;
