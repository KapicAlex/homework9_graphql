import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { MESSAGES_QUERY, POST_REPLY_MUTATION } from '../../queries';
import './replyInput.css'

const ReplyInput = ({showModal, messageId}) => {
  const [body, setBody] = useState("");

  const _updateStoreAfterAddingReply = (store, newReply, messageId) => {
    console.log(2222222)
    const orderBy = 'createdAt_DESC';
    const data = store.readQuery({
        query: MESSAGES_QUERY,
        variables: {
            orderBy
        }
    });
    const repliedMessage = data.messages.messageList.find(
        message => message.id === messageId
    );
    repliedMessage.replies.unshift(newReply);
    store.writeQuery({
        query: MESSAGES_QUERY,
        data
    });
  };

  return (
    <div className="reply-input">
      <h2 className="reply-input-header">Add your reply or
        <button
          className="reply-message-close"
          type="button"
          onClick={showModal}>
          Close
        </button>
      </h2>
      
      <Mutation
        mutation={POST_REPLY_MUTATION}
        variables={{ messageId, body }}
        update={(store, { data: { postReply } }) => {
          _updateStoreAfterAddingReply(store, postReply, messageId);
        }}
        onCompleted={() => showModal(false)}
        >
          {postMutation => <form className="reply-message-form" onSubmit={body ? postMutation : () => showModal(false)}>
                              <textarea
                                className="reply-message-input"
                                value={body}
                                onChange={e => setBody(e.target.value)} />
                              <div className="reply-message-buttons">
                                <button
                                  className="reply-message-button"
                                  type="submit">
                                  Make reply
                                </button>
                              </div>
                            </form>
          }
        </Mutation>
    </div>
  )
}

export default ReplyInput;