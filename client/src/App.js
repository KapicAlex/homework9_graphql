import './App.css';
import MessageList from "./components/MessageList/MessageList"

function App() {
  return (
    <div className="App">
      <MessageList />
    </div>
  );
}

export default App;
