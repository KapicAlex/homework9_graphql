function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
      body: args.body,
      likes: args.likes,
      dislikes: args.dislikes
  });
}

async function likeMessage(parent, args, context, info) {
  const id = args.messageId; 
  const messageExists = await context.prisma.$exists.message({ id });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  const message = await context.prisma.messages({ where: { id } });
  const likes = message[0].likes + 1;

  return context.prisma.updateMessage({ where: { id }, data: { likes } });
}

async function dislikeMessage(parent, args, context, info) {
  const id = args.messageId; 
  const messageExists = await context.prisma.$exists.message({ id });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  const message = await context.prisma.messages({ where: { id } });
  const dislikes = message[0].dislikes + 1;

  return context.prisma.updateMessage({ where: { id }, data: { dislikes } });
}

async function postReply(parent, args, context, info) {
  const id = args.messageId; 
  const messageExists = await context.prisma.$exists.message({ id });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.createReply({
    body: args.body,
    message: { connect: { id: args.messageId } }
  });
}

module.exports = {
  postMessage,
  likeMessage,
  dislikeMessage,
  postReply
}